# NOTE: Iteration
def fib(n):
    a = 0
    b = 1
    if n < 1:
        print(0)
    if n == 1:
        print(a)
    elif n > 1:
        for i in range(2, n+1):
            c = a+b
            a = b
            b = c
            if i == n:
                print(c)


fib(10)
fib(3)
# NOTE: recursion


def Fibonacci(val):
    if val > 1:
        return Fibonacci(val - 1) + Fibonacci(val - 2)
    else:
        return val


print(Fibonacci(10))
print(Fibonacci(3))
