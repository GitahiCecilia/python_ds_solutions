class plusOne:
    def solution(self, arr):
        n = len(arr) - 1
        while n >= 0:
            if arr[n] == 9:
                if n == 0:
                    arr[n] = 0
                    arr.insert(0, 1)
                    return arr
                else:
                    arr[n] = 0
            else:
                arr[n] = arr[n] + 1
                break
            n -= 1

        return arr


Onep = plusOne()
print(Onep.solution([9, 9]))
