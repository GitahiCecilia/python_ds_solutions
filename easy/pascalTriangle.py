class pascalTriangle:
    def solution(self, n):
        final = [[1]]
        prev = [1]
        i = 2
        while i <= n:
            newArr = [1]
            if i >= 2:
                k = len(prev)-1
                while k > 0:
                    newArr.append(prev[k]+prev[k-1])
                    k -= 1
            newArr.append(1)
            final.append(newArr)
            prev = newArr
            i += 1
        return final


app = pascalTriangle()
print(app.solution(5))
