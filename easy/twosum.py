# Return index of two values that add up to the given target
def twosum(nums, target):
    obj = {}
    for i in nums:
        diff = target - i
        if i in obj:
            return [nums.index(i), obj[i]]
        else:
            obj[diff] = nums.index(i)


print(twosum([7, 19, 2, 15], 9))
