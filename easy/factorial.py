# NOTE:Iteration
def factorial(n):
    a = 1
    for i in range(2, n+1):
        c = a * i
        a = c
        if i == n:
            print(a)


factorial(5)
# NOTE:recursion


def recurse(n):
    if n == 0:
        return 1
    return n * recurse(n-1)


print(recurse(5))
