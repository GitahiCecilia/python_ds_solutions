class pascalTriangle:
    def solution(self, n):
        prev = [1]
        i = 2
        while i <= n:
            newArr = [1]
            if i >= 2:
                k = len(prev)-1
                while k > 0:
                    newArr.append(prev[k]+prev[k-1])
                    k -= 1
            newArr.append(1)
            prev = newArr
            i += 1
        return prev


app = pascalTriangle()
print(app.solution(5))
