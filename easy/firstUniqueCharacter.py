class uniqueCharater:
    def unique(self, str):
        dict = {}
        for i in str:
            if i in dict:
                dict[i] += 1
            else:
                dict[i] = 1
        for idx, k in enumerate(str):
            if dict[k] == 1:
                return idx
        return -1


chara = uniqueCharater()
print(chara.unique("leetcode"))
print(chara.unique("loveleetcode"))
print(chara.unique("aabb"))
