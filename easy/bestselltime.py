class bestSell:
    def solution(self, arr):
        buy = arr[0]
        profit = 0
        for i in arr:
            if i > buy:
                prof = i - buy
                profit = max(prof, profit)
            else:
                buy = i
        return profit


be = bestSell()
print(be.solution([1, 4, 2]))
print(be.solution([7, 6, 4, 3, 1]))
print(be.solution([7, 1, 5, 3, 6, 4]))
print(be.solution([2, 1, 4]))
