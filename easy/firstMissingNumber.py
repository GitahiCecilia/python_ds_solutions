class MissingNumber:
    def solution(self, arr):
        arr.sort()
        i = 0
        while i < len(arr):
            if arr[i] >= 0:
                if i == len(arr) - 1:
                    return arr[i]+1
                elif arr[i+1] != arr[i]+1:
                    return arr[i]+1
            i += 1


miss = MissingNumber()
print(miss.solution([-1,-2,0,2]))
